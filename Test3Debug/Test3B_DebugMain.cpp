//This program tests various operation of a linked list
//45 67 23 89 -999

#include <iostream> 
#include "circularLinkedList.h" //DEBUG 01 - Fixed typo

using namespace std;

void testCopyConstructor(circularLinkedList<int> oList);

int main()
{
	circularLinkedList<int> list1, list2;			
	int num;										

	cout << "Enter some numbers ending with -999"
		 << endl;										
	cin >> num;										

	while (num != -999)				//DEBUG 09 - fixed typo, turned "num = -999" to "num != -999"
	{
		list1.insertNode(num);						
		cin >> num;									
	}

	cout << endl;										

	cout << "List 1: ";
    list1.print();
    cout << endl;			

	cout << "Length List 1: " << list1.length() << endl;    			

	cout << "Enter the number to be searched:  "; //DEBUG 02 - Fixed typo - moved ';' out of quotes
	cin >> num;										
	cout << endl;										

	if (list1.search(num))							
		cout << num << " found in the list" << endl;									
	else											
		cout << num << " not in the list" << endl;	

	cout << "Enter the number to be deleted: ";								
	cin >> num;										
	cout << endl;										

	list1.deleteNode(num);	//DEBUG 03 - Fixed typo, changed "num1" to "num"
	
	cout << "After deleting the node, "
		 << "List 1: ";
    list1.print();
    cout << endl;					

	cout << "Length List 1: " << list1.length() << endl;		//DEBUG 04 - Added "list1." to left of "length()"

	list2 = list1;	   

	cout << "List 2: ";
    list2.print();
    cout << endl;	

	cout << "Length List 2: " << list2.length() << endl;									

	testCopyConstructor(list1);						

	cout << "List 1: "; //DEBUG 05 - Added missing ';'
    list1.print();
    cout << endl;	
    
	cin >> num;										

	return 0;			 							
}

void testCopyConstructor(circularLinkedList<int> oList)
{
}

